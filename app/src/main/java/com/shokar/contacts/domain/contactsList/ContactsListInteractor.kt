package com.shokar.contacts.domain.contactsList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shokar.contacts.domain.models.ContactModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ContactsListInteractor(private val contactsListData: ContactsListDataInterface) :
    ContactsListInteractorInterface {

    override fun getContacts(text: String?): LiveData<List<ContactModel>> {
        return contactsListData.getContacts(text)
    }
}