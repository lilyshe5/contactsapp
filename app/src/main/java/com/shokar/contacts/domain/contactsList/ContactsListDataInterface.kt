package com.shokar.contacts.domain.contactsList

import androidx.lifecycle.LiveData
import com.shokar.contacts.domain.models.ContactModel

interface ContactsListDataInterface {

    fun getContacts(text: String?): LiveData<List<ContactModel>>
}