package com.shokar.contacts.domain.contactEdit

import com.shokar.contacts.domain.models.ContactModel

interface ContactDataInterface {
    fun getContact(id: Long): ContactModel

    fun saveContact(contact: ContactModel): Int

    fun updateContact(contact: ContactModel): Int

    fun deleteContact(contact: ContactModel): Int
}