package com.shokar.contacts.domain.contactsList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shokar.contacts.domain.models.ContactModel

interface ContactsListInteractorInterface {

    fun getContacts(text: String?): LiveData<List<ContactModel>>
}