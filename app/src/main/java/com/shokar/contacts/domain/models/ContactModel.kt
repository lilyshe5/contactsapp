package com.shokar.contacts.domain.models

data class ContactModel(
    val id: Long,
    val icon: String?,
    val firstName: String,
    val lastName: String?,
    val phoneNumber: String,
    val phoneRingtone: String,
    val note: String?
)