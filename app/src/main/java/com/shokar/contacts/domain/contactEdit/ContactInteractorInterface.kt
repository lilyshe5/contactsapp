package com.shokar.contacts.domain.contactEdit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shokar.contacts.domain.models.ContactModel

interface ContactInteractorInterface {
    val contact: MutableLiveData<ContactModel>
    val response: MutableLiveData<Int>

    fun getContact(id: Long): LiveData<ContactModel>

    fun saveContact(contact: ContactModel): LiveData<Int>

    fun updateContact(contact: ContactModel): LiveData<Int>

    fun deleteContact(contact: ContactModel): LiveData<Int>

}