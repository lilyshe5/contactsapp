package com.shokar.contacts.domain.contactEdit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shokar.contacts.domain.models.ContactModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ContactInteractor(private val contactData: ContactDataInterface): ContactInteractorInterface {

    override val contact: MutableLiveData<ContactModel> by lazy {
        MutableLiveData<ContactModel>()
    }

    override val response: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    override fun getContact(id: Long): LiveData<ContactModel> {
        CoroutineScope(Dispatchers.IO + Job()).launch {
            contact.postValue(contactData.getContact(id))
        }
        return contact
    }

    override fun saveContact(contact: ContactModel): LiveData<Int> {
        CoroutineScope(Dispatchers.IO + Job()).launch {
            response.postValue(contactData.saveContact(contact))
        }

        return response
    }

    override fun updateContact(contact: ContactModel): LiveData<Int> {
        CoroutineScope(Dispatchers.IO + Job()).launch {
            response.postValue(contactData.updateContact(contact))
        }

        return response
    }

    override fun deleteContact(contact: ContactModel): LiveData<Int> {
        CoroutineScope(Dispatchers.IO + Job()).launch {
            response.postValue(contactData.deleteContact(contact))
        }

        return response
    }

}