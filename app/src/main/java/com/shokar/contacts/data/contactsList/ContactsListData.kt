package com.shokar.contacts.data.contactsList

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.shokar.contacts.data.db.ContactDao
import com.shokar.contacts.data.ContactModelMapper.mapListContactModel
import com.shokar.contacts.domain.contactsList.ContactsListDataInterface
import com.shokar.contacts.domain.models.ContactModel

class ContactsListData(private val database: ContactDao):
    ContactsListDataInterface {
    override fun getContacts(text: String?): LiveData<List<ContactModel>> =
        Transformations.map(database.getAll(text)) { mapListContactModel(it)}
}