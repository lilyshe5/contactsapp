package com.shokar.contacts.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ContactEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "icon") val icon: String?,
    @ColumnInfo(name = "first_name") val firstName: String,
    @ColumnInfo(name = "last_name") val lastName: String?,
    @ColumnInfo(name = "phone_number") val phoneNumber: String,
    @ColumnInfo(name = "phone_ringtone") val phoneRingtone: String,
    @ColumnInfo(name = "note") val note: String?
)