package com.shokar.contacts.data.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ContactDao {
    @Query ("SELECT * FROM ContactEntity WHERE first_name LIKE '%'||:text||'%' ")
    fun getAll(text: String?): LiveData<List<ContactEntity>>

    @Query("SELECT * FROM ContactEntity WHERE id = (:contactId)")
    fun getContact(contactId: Long): ContactEntity

    @Insert
    fun insertContact(contact: ContactEntity): Long

    @Update
    fun updateContact(contact: ContactEntity): Int

    @Delete
    fun deleteContact(contact: ContactEntity): Int
}