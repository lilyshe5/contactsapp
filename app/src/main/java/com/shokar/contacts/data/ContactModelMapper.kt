package com.shokar.contacts.data

import com.shokar.contacts.data.db.ContactEntity
import com.shokar.contacts.domain.models.ContactModel

object ContactModelMapper {

    fun mapListContactModel(contactEntities: List<ContactEntity>?): List<ContactModel> {

        val contactModel = mutableListOf<ContactModel>()

        if (contactEntities != null) {
            for (contact in contactEntities) {
                contactModel.add(mapContactModel(contact))
            }
        }

        return contactModel
    }

    fun mapContactModel(contactEntity: ContactEntity): ContactModel {
        return ContactModel(
            contactEntity.id,
            contactEntity.icon,
            contactEntity.firstName,
            contactEntity.lastName,
            contactEntity.phoneNumber,
            contactEntity.phoneRingtone,
            contactEntity.note
        )
    }

    fun mapContactEntity(contactModel: ContactModel): ContactEntity {

        return ContactEntity(
            contactModel.id,
            contactModel.icon,
            contactModel.firstName,
            contactModel.lastName,
            contactModel.phoneNumber,
            contactModel.phoneRingtone,
            contactModel.note
        )

    }

}