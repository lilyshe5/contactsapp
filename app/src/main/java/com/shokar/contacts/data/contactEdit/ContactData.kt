package com.shokar.contacts.data.contactEdit

import com.shokar.contacts.data.ContactModelMapper
import com.shokar.contacts.data.db.ContactDao
import com.shokar.contacts.domain.contactEdit.ContactDataInterface
import com.shokar.contacts.domain.models.ContactModel

class ContactData(private val database: ContactDao): ContactDataInterface {

    override fun getContact(id: Long): ContactModel {
        return ContactModelMapper.mapContactModel(database.getContact(id))
    }

    override fun saveContact(contact: ContactModel): Int {
        return if(database.insertContact(ContactModelMapper.mapContactEntity(contact)) == 0L)
            RESPONSE_FAILED
        else
            RESPONSE_FINE
    }

    override fun updateContact(contact: ContactModel): Int {
        return if (database.updateContact(ContactModelMapper.mapContactEntity(contact)) == 0)
            RESPONSE_FAILED
        else
            RESPONSE_FINE
    }

    override fun deleteContact(contact: ContactModel): Int {
        return if (database.deleteContact(ContactModelMapper.mapContactEntity(contact)) == 0)
            RESPONSE_FAILED
        else
            RESPONSE_FINE
    }

    companion object{
        const val RESPONSE_FAILED = 0
        const val RESPONSE_FINE = 1
    }
}