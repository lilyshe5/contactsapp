package com.shokar.contacts

import android.app.Application
import com.shokar.contacts.di.Modules.contactViewModel
import com.shokar.contacts.di.Modules.contactsListViewModel
import com.shokar.contacts.di.Modules.dbModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ContactsApplication : Application(){

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@ContactsApplication)
            modules(listOf(dbModel, contactsListViewModel, contactViewModel))
        }
    }

}