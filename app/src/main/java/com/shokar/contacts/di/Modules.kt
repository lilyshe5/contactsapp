package com.shokar.contacts.di

import androidx.room.Room
import com.shokar.contacts.R
import com.shokar.contacts.data.contactEdit.ContactData
import com.shokar.contacts.data.db.ContactDatabase
import com.shokar.contacts.data.contactsList.ContactsListData
import com.shokar.contacts.domain.contactEdit.ContactDataInterface
import com.shokar.contacts.domain.contactEdit.ContactInteractor
import com.shokar.contacts.domain.contactEdit.ContactInteractorInterface
import com.shokar.contacts.domain.contactsList.ContactsListDataInterface
import com.shokar.contacts.domain.contactsList.ContactsListInteractor
import com.shokar.contacts.domain.contactsList.ContactsListInteractorInterface
import com.shokar.contacts.presentaion.contactEdit.ContactViewModel
import com.shokar.contacts.presentaion.contactsList.ContactsListViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Modules {

    val dbModel = module {
        single {
            Room.databaseBuilder(
                androidApplication(),
                ContactDatabase::class.java,
                androidApplication().resources.getString(
                    R.string.database_contacts_name
                )
            )
                .fallbackToDestructiveMigration()
                .build().contactDao()
        }
    }

    val contactsListViewModel = module {
        single<ContactsListDataInterface> { ContactsListData(get()) }
        single<ContactsListInteractorInterface> { ContactsListInteractor(get()) }
        viewModel { ContactsListViewModel(get()) }
    }

    val contactViewModel = module {
        single<ContactDataInterface> { ContactData(get()) }
        single<ContactInteractorInterface> { ContactInteractor(get()) }
        viewModel { ContactViewModel(get()) }
    }


}