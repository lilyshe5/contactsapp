package com.shokar.contacts.presentaion.contactsList.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter
import com.shokar.contacts.BR
import com.shokar.contacts.R
import com.shokar.contacts.presentaion.contactsList.models.RecyclerViewContent
import com.shokar.contacts.presentaion.contactsList.models.RecyclerViewSection

class ContactsListAdapter(
    private val context: Context,
    private val sections: MutableList<RecyclerViewSection>,
    private val onItemClick: (view: Long) -> Unit
) :
    SectionRecyclerViewAdapter<RecyclerViewSection, RecyclerViewContent, HeaderViewHolder, ContentViewHolder>(
        context,
        sections
    ) {
    override fun onCreateSectionViewHolder(
        headerViewGroup: ViewGroup?,
        viewType: Int
    ): HeaderViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.recycler_view_header_contacts, headerViewGroup, false)
        return HeaderViewHolder(view)
    }

    override fun onCreateChildViewHolder(
        contentViewGroup: ViewGroup?,
        viewType: Int
    ): ContentViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.recycler_view_content_contacts, contentViewGroup, false)

        return ContentViewHolder(view)
    }

    override fun onBindSectionViewHolder(
        headerViewHolder: HeaderViewHolder?,
        headerPosition: Int,
        section: RecyclerViewSection?
    ) {
        headerViewHolder?.bindItem(sections[headerPosition].sectionName)
    }

    override fun onBindChildViewHolder(
        contentViewHolder: ContentViewHolder?,
        headerPosition: Int,
        contentPosition: Int,
        content: RecyclerViewContent?
    ) {
        val currentSection = sections[headerPosition].childItems[contentPosition].contact
        contentViewHolder?.bindItem(currentSection.id, currentSection.profileName, currentSection.profileIcon.toUri())

        contentViewHolder?.itemView?.setOnClickListener {
            onItemClick(currentSection.id)
        }

    }
}