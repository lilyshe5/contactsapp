package com.shokar.contacts.presentaion.contactsList.models

data class ContactsListModel(
    val id: Long,
    val profileName: String,
    val profileIcon: String
)