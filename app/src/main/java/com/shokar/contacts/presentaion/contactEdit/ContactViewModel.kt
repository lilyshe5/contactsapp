package com.shokar.contacts.presentaion.contactEdit

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.shokar.contacts.R
import com.shokar.contacts.data.contactEdit.ContactData.Companion.RESPONSE_FAILED
import com.shokar.contacts.data.contactEdit.ContactData.Companion.RESPONSE_FINE
import com.shokar.contacts.domain.contactEdit.ContactInteractorInterface
import com.shokar.contacts.domain.models.ContactModel
import com.shokar.contacts.presentaion.Event


class ContactViewModel(private val repository: ContactInteractorInterface) : ViewModel() {

    fun contact(): LiveData<ContactModel> = repository.contact

    fun contact(id: Long): LiveData<ContactModel> {
        return repository.getContact(id)
    }

    fun saveContact(contact: ContactModel): LiveData<Event<Int>> =
        Transformations.map(repository.saveContact(contact)) {
            Event(convertResponse(it, R.string.database_save_failed, R.string.database_save_fine))
        }

    fun updateContact(contact: ContactModel): LiveData<Event<Int>> =
        Transformations.map(repository.updateContact(contact)) {
            Event(convertResponse(it, R.string.database_update_failed, R.string.database_update_fine))
        }

    fun deleteContact(contact: ContactModel): LiveData<Event<Int>> =
        Transformations.map(repository.deleteContact(contact)){
            Event(convertResponse(it, R.string.database_delete_failed, R.string.database_delete_fine))
        }

    private fun convertResponse(response: Int, failed: Int, success: Int): Int {
        return when (response) {
            RESPONSE_FAILED -> failed
            RESPONSE_FINE -> success
            else -> 0
        }
    }

}