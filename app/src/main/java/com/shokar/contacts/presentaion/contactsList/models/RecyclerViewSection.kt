package com.shokar.contacts.presentaion.contactsList.models

import com.intrusoft.sectionedrecyclerview.Section

class RecyclerViewSection(
    private val contentList: MutableList<RecyclerViewContent>,
    val sectionName: String
) : Section<RecyclerViewContent> {

    override fun getChildItems(): MutableList<RecyclerViewContent> {
        return contentList
    }
}