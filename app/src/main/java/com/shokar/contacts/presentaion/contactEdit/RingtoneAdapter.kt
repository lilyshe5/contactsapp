package com.shokar.contacts.presentaion.contactEdit

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.shokar.contacts.R
import com.shokar.contacts.presentaion.contactEdit.models.RingtoneModel
import kotlinx.android.synthetic.main.dialog_fragment_ringtone.view.*
import kotlinx.android.synthetic.main.ringtone_item.view.*

class RingtoneAdapter(
    private val context: Context,
    private val ringtone: MutableList<RingtoneModel>
) :
    RecyclerView.Adapter<RingtoneAdapter.ViewHolder>() {

    private var selectedRingtone: RadioButton? = null

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(name: String) {
            view.radioButtonRingtone.text = name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.ringtone_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(ringtone[position].ringtoneName)

        holder.itemView.radioButtonRingtone.isChecked = ringtone[position].isChecked

        holder.itemView.radioButtonRingtone.setOnClickListener {
            for (i in ringtone)
                i.isChecked = false

            ringtone[position].isChecked = true

            selectedRingtone = it as RadioButton
            selectedRingtone?.isChecked = true

            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return ringtone.size
    }
}