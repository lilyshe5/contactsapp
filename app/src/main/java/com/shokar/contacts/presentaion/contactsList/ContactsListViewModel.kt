package com.shokar.contacts.presentaion.contactsList

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.shokar.contacts.domain.contactsList.ContactsListInteractorInterface
import com.shokar.contacts.domain.models.ContactModel
import com.shokar.contacts.presentaion.contactsList.RecyclerViewSectionMapper.mapRecyclerViewSection
import com.shokar.contacts.presentaion.contactsList.models.RecyclerViewSection

class ContactsListViewModel(private val repository: ContactsListInteractorInterface) : ViewModel() {

    fun contacts(text: String?): LiveData<MutableList<RecyclerViewSection>> =
        Transformations.map(repository.getContacts(text)) {
            mapRecyclerViewSection(it)
        }
}