package com.shokar.contacts.presentaion.contactEdit

import android.app.Dialog
import android.content.Context
import android.database.Cursor
import android.media.RingtoneManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shokar.contacts.R
import com.shokar.contacts.presentaion.contactEdit.models.RingtoneModel


class RingtonePicker : DialogFragment() {

    private lateinit var ringtones: MutableList<RingtoneModel>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.dialog_fragment_ringtone, container, false)

        ringtones = getPhoneRingtones()

        initAdapter(view)
        initListeners(view)

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(STYLE_NO_TITLE, R.style.DialogStyle)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

        return dialog
    }

    private fun initAdapter(view: View?) {
        val linearLayoutManager = LinearLayoutManager(context)
        val ringtone = view?.findViewById<RecyclerView>(R.id.ringtoneList)

        ringtone?.layoutManager = linearLayoutManager
        ringtone?.adapter =
            context?.let {
                RingtoneAdapter(
                    it,
                    ringtones
                )
            }
    }

    private fun initListeners(view: View?){
        val buttonAccept = view?.findViewById<Button>(R.id.buttonAccept)
        buttonAccept?.setOnClickListener {
            for (i in ringtones)
                if (i.isChecked) {
                    val buttonRingtone = activity?.findViewById<Button>(R.id.buttonRingtone)
                    buttonRingtone?.text = i.ringtoneName
                    this@RingtonePicker.dismiss()
                }
        }

        val buttonCancel = view?.findViewById<Button>(R.id.buttonCancel)
        buttonCancel?.setOnClickListener {
            this@RingtonePicker.dismiss()
        }
    }

    private fun getPhoneRingtones(): MutableList<RingtoneModel>{
        val outData = mutableListOf<RingtoneModel>()

        val manager = RingtoneManager(context)
        manager.setType(RingtoneManager.TYPE_RINGTONE)
        val cursor: Cursor = manager.cursor

        while (cursor.moveToNext()) {
            outData.add(RingtoneModel(cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX)))
        }
        return outData
    }

}