package com.shokar.contacts.presentaion.contactsList.adapters

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_view_header_contacts.view.*

class HeaderViewHolder(private val view: View): RecyclerView.ViewHolder(view) {

    fun bindItem(groupName: String){
        view.textSectionName.text = groupName
    }

}