package com.shokar.contacts.presentaion.contactsList

import androidx.lifecycle.LiveData
import com.shokar.contacts.domain.models.ContactModel
import com.shokar.contacts.presentaion.contactsList.models.RecyclerViewContent
import com.shokar.contacts.presentaion.contactsList.models.RecyclerViewSection
import com.shokar.contacts.presentaion.contactsList.models.ContactsListModel

object RecyclerViewSectionMapper {

    fun mapRecyclerViewSection(contacts: List<ContactModel>): MutableList<RecyclerViewSection> {

        val outData = mutableListOf<RecyclerViewSection>()
        if (contacts.isNotEmpty()) {
            contacts.sortedBy { item -> item.firstName }

            val sectionedList = mutableListOf<RecyclerViewContent>()
            var currentSection = contacts.first().firstName[0]

            for (contact in contacts) {
                if (contact.firstName[0] == currentSection) {
                    sectionedList.add(addContent(contact))
                } else {
                    outData.add(
                        RecyclerViewSection(
                            sectionedList.toMutableList(),
                            currentSection.toString()
                        )
                    )

                    currentSection = contact.firstName[0]
                    sectionedList.clear()

                    sectionedList.add(addContent(contact))
                }
            }
            outData.add(
                RecyclerViewSection(
                    sectionedList,
                    currentSection.toString()
                )
            )
        }

        return outData
    }

    private fun addContent(contact: ContactModel): RecyclerViewContent {
        return RecyclerViewContent(
            ContactsListModel(
                contact.id,
                contact.firstName,
                contact.icon.toString()
            )
        )
    }
}