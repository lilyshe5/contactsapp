package com.shokar.contacts.presentaion.contactsList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.shokar.contacts.R
import com.shokar.contacts.presentaion.contactEdit.ContactActivity.Companion.createContactIntent
import com.shokar.contacts.presentaion.contactEdit.ContactActivity.Companion.editContactIntent
import com.shokar.contacts.presentaion.contactsList.adapters.ContactsListAdapter
import com.shokar.contacts.presentaion.contactsList.models.RecyclerViewSection
import kotlinx.android.synthetic.main.activity_contacts_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContactsListActivity : AppCompatActivity() {

    private val model: ContactsListViewModel by viewModel()
    private val onItemClick =
        { id: Long ->
            startActivity(editContactIntent(this, id))
        }

    private val observer = Observer<MutableList<RecyclerViewSection>> { data ->
        val linearLayoutManager = LinearLayoutManager(this)
        contactsList.layoutManager = linearLayoutManager
        contactsList.setHasFixedSize(false)

        contactsList.adapter = ContactsListAdapter(this, data, onItemClick)

        (contactsList.adapter as ContactsListAdapter).notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts_list)

        initAdapter()
        initListeners()
    }

    override fun onResume() {
        super.onResume()

        if (contactsList.adapter != null)
            (contactsList.adapter as ContactsListAdapter).notifyDataSetChanged()
    }

    private fun initAdapter() {
        model.contacts("").observe(this, observer)
    }

    private fun initListeners() {
        fabAddContact.setOnClickListener {
            startActivity(createContactIntent(this))
        }

        textFirstName.addTextChangedListener {
            model.contacts(it.toString()).observe(this, observer)
        }
    }
}
