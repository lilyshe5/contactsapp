package com.shokar.contacts.presentaion.contactsList.adapters

import android.net.Uri
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.recycler_view_content_contacts.view.*

class ContentViewHolder(
    private val view: View
) : RecyclerView.ViewHolder(view) {
    private var id: Long = 0L

    fun bindItem(id: Long, name: String, profileIcon: Uri) {
        this.id = id
        view.textProfileName.text = name

        Glide
            .with(view.profileIcon.context)
            .load(profileIcon)
            .apply(RequestOptions().optionalCircleCrop())
            .into(view.profileIcon)
    }
}