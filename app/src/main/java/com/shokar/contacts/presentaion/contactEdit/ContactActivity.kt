package com.shokar.contacts.presentaion.contactEdit

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.shokar.contacts.R
import com.shokar.contacts.databinding.ActivityContactBinding
import com.shokar.contacts.domain.models.ContactModel
import com.shokar.contacts.presentaion.Event
import kotlinx.android.synthetic.main.activity_contact.*
import kotlinx.android.synthetic.main.bottom_sheet.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class ContactActivity : AppCompatActivity() {

    private val model: ContactViewModel by viewModel()
    private var action: String? = ""
    private var id: Long = 0L
    private var uri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityContactBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_contact)

        binding.run {
            lifecycleOwner = this@ContactActivity
        }

        action = intent.getStringExtra(ACTION)

        when (action) {

            ACTION_CREATE -> buttonDelete.visibility = View.GONE

            ACTION_EDIT -> {
                binding.viewModel = model
                id = intent.getLongExtra(ID, 0L)

                model.contact(id).observe(this, Observer { contact ->
                    uri = contact.icon?.toUri()
                    uri?.let { loadImage(profileIcon, it) }
                })
            }
        }

        initListeners()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            IMAGE_PICK_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    uri = data?.data
                    uri?.let { loadImage(profileIcon, it) }
                }
            }
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    uri?.let { loadImage(profileIcon, it) }
                }
            }
        }
    }

    private fun initListeners() {

        //Ringtone button
        buttonRingtone.setOnClickListener {

            val ringtonePicker = RingtonePicker()
            ringtonePicker.show(
                supportFragmentManager,
                resources.getString(R.string.dialog_fragment_tag)
            )
        }

        //Bottom panel
        val bottomSheet = BottomSheetBehavior.from(bottomSheet)
        bottomSheet.state = BottomSheetBehavior.STATE_HIDDEN

        bottomSheet.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
            override fun onStateChanged(bottomSheet: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_HIDDEN -> coordinatorLayout.visibility = View.GONE
                }
            }
        })

        buttonGallery.setOnClickListener {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSION_READ_STORAGE)
            } else {
                loadPictureFromGallery()
            }
        }

        buttonCamera.setOnClickListener {
            when {
                checkSelfPermission(Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_DENIED -> {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), PERMISSION_CAMERA)
                }
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED -> {
                    requestPermissions(
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        PERMISSION_WRITE_STORAGE
                    )
                }
                else -> loadPictureFromCamera()
            }
        }

        //Icon click
        profileIcon.setOnClickListener {
            hideKeyboard()

            coordinatorLayout.visibility = View.VISIBLE

            bottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
        }

        Observer<Event<Int>> {
            if (!it.hasBeenHandled) {
                Toast.makeText(
                    applicationContext,
                    resources.getString(it.peekContent()),
                    Toast.LENGTH_SHORT
                ).show()
            }
            it.getContentIfNotHandled()

            this@ContactActivity.finish()
        }.also { observer ->

            //Delete contact
            buttonDelete.setOnClickListener {

                val builder = AlertDialog.Builder(this)
                builder.setTitle(resources.getString(R.string.text_delete))
                builder.setMessage(
                    String.format(
                        resources.getString(R.string.text_confirm_delete),
                        model.contact().value?.firstName
                    )
                )

                builder.setPositiveButton(R.string.text_yes) { _, _ ->
                    val requiredFields = checkRequiredFields()
                    if (requiredFields.isEmpty()) {
                        model.deleteContact(ContactModel(id, "", "", "", "", "", ""))
                            .observe(this, observer)
                    } else {
                        Toast.makeText(
                            this,
                            resources.getString(R.string.text_required) + requiredFields,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                builder.setNegativeButton(R.string.text_no) {_,_ -> }
                builder.show()
            }

            //Save contact
            fabSaveContact.setOnClickListener {

                when (action) {
                    ACTION_EDIT -> {
                        val requiredFields = checkRequiredFields()
                        if (requiredFields.isEmpty()) {
                            model.updateContact(getContactInfo()).observe(this, observer)
                        } else {
                            Toast.makeText(
                                this,
                                resources.getString(R.string.text_required) + requiredFields,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    ACTION_CREATE -> {
                        val requiredFields = checkRequiredFields()
                        if (requiredFields.isEmpty()) {
                            model.saveContact(getContactInfo()).observe(this, observer)
                        } else {
                            Toast.makeText(
                                this,
                                resources.getString(R.string.text_required) + requiredFields,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }
    }

    private fun checkRequiredFields(): String {
        var outData = ""

        if (textFirstName.text.toString().isEmpty())
            outData += resources.getString(R.string.required_first_name) + " "
        if (textPhoneNumber.text.toString().isEmpty())
            outData += resources.getString(R.string.required_phone_number) + " "

        return outData
    }

    private fun getContactInfo(): ContactModel {
        return ContactModel(
            id,
            uri?.toString(),
            textFirstName.text.toString(),
            textLastName.text.toString(),
            textPhoneNumber.text.toString(),
            buttonRingtone.text.toString(),
            textNote.text.toString()
        )
    }

    private fun loadPictureFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    private fun loadPictureFromCamera() {
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        uri = contentResolver
            .insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values
            )
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION
                        or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            )
            startActivityForResult(intent, CAMERA_REQUEST_CODE)
        }
    }

    private fun loadImage(view: ImageView, uri: Uri) {
        Glide
            .with(view.context)
            .load(uri)
            .apply(
                RequestOptions().optionalCircleCrop()
            )
            .into(profileIcon)
    }

    private fun hideKeyboard(){
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(profileIcon.windowToken, 0)
        imm.showSoftInput(profileIcon, InputMethodManager.SHOW_IMPLICIT)
    }

    companion object {

        private const val PERMISSION_READ_STORAGE = 0
        private const val PERMISSION_WRITE_STORAGE = 1
        private const val PERMISSION_CAMERA = 2

        private const val IMAGE_PICK_CODE = 1000
        private const val CAMERA_REQUEST_CODE = 1001

        private const val ACTION = "Action"
        private const val ID = "Id"

        private const val ACTION_CREATE = "Create"
        private const val ACTION_EDIT = "Edit"

        fun createContactIntent(context: Context): Intent {
            val intent = Intent(context, ContactActivity::class.java)
            intent.putExtra(ACTION, ACTION_CREATE)
            return intent
        }

        fun editContactIntent(context: Context, id: Long): Intent {
            val intent = Intent(context, ContactActivity::class.java)
            intent.putExtra(ACTION, ACTION_EDIT)
            intent.putExtra(ID, id)
            return intent
        }

    }
}