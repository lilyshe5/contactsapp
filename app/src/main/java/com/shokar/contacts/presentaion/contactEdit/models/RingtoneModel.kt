package com.shokar.contacts.presentaion.contactEdit.models

data class RingtoneModel(
    val ringtoneName: String,
    var isChecked: Boolean = false
)